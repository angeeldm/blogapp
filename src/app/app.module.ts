import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './pages/users/users.component';
import { RestService } from './services/rest.service';
import { PostsComponent } from './pages/posts/posts.component';
import { AlbumsComponent } from './pages/albums/albums.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { UserDetailComponent } from './pages/users/user-detail/user-detail.component';
import { AlbumDetailComponent } from './pages/albums/album-detail/album-detail.component';
import { TaskDetailComponent } from './pages/tasks/task-detail/task-detail.component';
import { PostsDetailComponent } from './pages/posts/posts-detail/posts-detail.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { PostsByUserComponent } from './pages/posts/posts-by-user/posts-by-user.component';
import { CommentsComponent } from './pages/comments/comments.component';
import { AlbumsByUserComponent } from './pages/albums/albums-by-user/albums-by-user.component';
import { PhotosComponent } from './pages/photos/photos.component';
import { TasksByUserComponent } from './pages/tasks/tasks-by-user/tasks-by-user.component';
import { TextsPipe } from './texts.pipe';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    AlbumsComponent,
    TasksComponent,
    UserDetailComponent,
    AlbumDetailComponent,
    TaskDetailComponent,
    PostsDetailComponent,
    HeaderComponent,
    FooterComponent,
    PostsByUserComponent,
    CommentsComponent,
    AlbumsByUserComponent,
    PhotosComponent,
    TasksByUserComponent,
    TextsPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    RestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
