import { CommentsComponent } from './pages/comments/comments.component';
import { PostsByUserComponent } from './pages/posts/posts-by-user/posts-by-user.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { AlbumsComponent } from './pages/albums/albums.component';
import { PostsComponent } from './pages/posts/posts.component';
import { UsersComponent } from './pages/users/users.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserDetailComponent } from './pages/users/user-detail/user-detail.component';
import { AlbumDetailComponent } from './pages/albums/album-detail/album-detail.component';
import { TaskDetailComponent } from './pages/tasks/task-detail/task-detail.component';
import { PostsDetailComponent } from './pages/posts/posts-detail/posts-detail.component';
import { AlbumsByUserComponent } from './pages/albums/albums-by-user/albums-by-user.component';
import { PhotosComponent } from './pages/photos/photos.component';
import { TasksByUserComponent } from './pages/tasks/tasks-by-user/tasks-by-user.component';

const routes: Routes = [
  { path: '', component: UsersComponent },
  { path: 'user-detail/:id', component: UserDetailComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'posts-by-user/:id', component: PostsByUserComponent },
  { path: 'post-detail/:id', component: PostsDetailComponent },
  { path: 'comments/:id', component: CommentsComponent },
  { path: 'albums', component: AlbumsComponent },
  { path: 'albums-by-user/:id', component: AlbumsByUserComponent },
  { path: 'albums-detail/:id', component: AlbumDetailComponent },
  { path: 'galery/:id', component: PhotosComponent },
  { path: 'todos', component: TasksComponent },
  { path: 'user-todos/:id', component: TasksByUserComponent },
  { path: 'todo-detail/:id', component: TaskDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
