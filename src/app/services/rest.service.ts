import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) {
    
  }

  get(url:string) {
    return this.http.get(url);
  }

  getUser(id:number){
    return this.http.get(`https://jsonplaceholder.typicode.com/users/${id}`)
  }
}
