import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RestService } from '../../services/rest.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  id:any;
  commentsList:any = [];

  constructor(private RestService: RestService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getComments().subscribe(res => this.commentsList = res);
  }

  getComments() : Observable<any>{
    return this.RestService.get(`https://jsonplaceholder.typicode.com/posts/${this.id}/comments`);
  }

}
