import { RestService } from '../../services/rest.service';
import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/posts';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  postsList: Array<Post> = [];

  constructor(private RestService: RestService) { }

  ngOnInit(): void {
    this.getPostByUser().subscribe(res => this.postsList = res);
  }

  getPostByUser() : Observable <any> {
    return this.RestService.get('https://jsonplaceholder.typicode.com/posts')
  }

}
