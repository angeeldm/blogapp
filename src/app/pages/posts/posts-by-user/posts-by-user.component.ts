import { Component, OnInit } from '@angular/core';
import { Post } from '../../../models/posts';
import { Observable } from 'rxjs';
import { RestService } from '../../../services/rest.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-posts-by-user',
  templateUrl: './posts-by-user.component.html',
  styleUrls: ['./posts-by-user.component.scss']
})
export class PostsByUserComponent implements OnInit {
  id:any;
  postsByUser: Array<Post> = [];
  user:any;

  constructor(private RestService: RestService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getPostsByUser().subscribe(res => this.postsByUser = res);
    this.RestService.getUser(this.id).subscribe(res => this.user = res);
  }
  
  getPostsByUser(): Observable <any>{
    return this.RestService.get(`https://jsonplaceholder.typicode.com/users/${this.id}/posts`);
  }

}
