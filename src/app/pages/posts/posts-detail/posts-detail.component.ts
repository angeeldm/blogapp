import { Post } from '../../../models/posts';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RestService } from '../../../services/rest.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts-detail',
  templateUrl: './posts-detail.component.html',
  styleUrls: ['./posts-detail.component.scss']
})
export class PostsDetailComponent implements OnInit {
  id:any;
  postDetail:any = {} as Post;

  constructor(private RestService: RestService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getPostsByUser().subscribe(res => this.postDetail = res);
  }
  
  getPostsByUser(): Observable <any>{
    return this.RestService.get(`https://jsonplaceholder.typicode.com/posts/${this.id}`);
  }

}
