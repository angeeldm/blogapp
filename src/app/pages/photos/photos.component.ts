import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RestService } from '../../services/rest.service';
import { Component, OnInit } from '@angular/core';
import { Galery } from '../../models/galery';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {
  id:any;
  galery:Array<Galery> = [];

  constructor(private RestService: RestService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getGalery().subscribe(res => this.galery = res);    

  }

  getGalery(): Observable<any> {
    return this.RestService.get(`https://jsonplaceholder.typicode.com/albums/${this.id}/photos`)
  }

  delete(item:number){
    const galeryFilter = this.galery.filter(elemento => elemento.id !== item);
    this.galery = galeryFilter;
  }
}