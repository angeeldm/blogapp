import { RestService } from '../../services/rest.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Album } from '../../models/albums';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit {
  albumsList: Array<Album> = [];

  constructor(private RestService:RestService) { }

  ngOnInit(): void {
    this.getAlbumsByUser().subscribe(res => this.albumsList = res);
  }

  getAlbumsByUser() : Observable<any>{
    return this.RestService.get(`https://jsonplaceholder.typicode.com/albums`);
  }

}
