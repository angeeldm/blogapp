import { RestService } from '../../../services/rest.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Album } from 'src/app/models/albums';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.scss']
})
export class AlbumDetailComponent implements OnInit {
  id:any;
  album:any = {} as Album;

  constructor(private RestService:RestService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;    
    this.getAlbumsByUser().subscribe(res => this.album = res);
  }

  getAlbumsByUser(): Observable<any>{
    return this.RestService.get(`https://jsonplaceholder.typicode.com/albums/${this.id}`);
  }

}
