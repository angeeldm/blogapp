import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumsByUserComponent } from './albums-by-user.component';

describe('AlbumsByUserComponent', () => {
  let component: AlbumsByUserComponent;
  let fixture: ComponentFixture<AlbumsByUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlbumsByUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumsByUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
