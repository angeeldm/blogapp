import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RestService } from '../../../services/rest.service';
import { Component, OnInit } from '@angular/core';
import { Album } from 'src/app/models/albums';

@Component({
  selector: 'app-albums-by-user',
  templateUrl: './albums-by-user.component.html',
  styleUrls: ['./albums-by-user.component.scss']
})
export class AlbumsByUserComponent implements OnInit {
  id:any;
  albumsList: Array<Album> = [];
  user:any;

  constructor(private RestService: RestService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getAlbumsByUser().subscribe(res => this.albumsList = res );
    this.getUser().subscribe(res => this.user = res);
  }

  getAlbumsByUser(): Observable<any> {
    return this.RestService.get(`https://jsonplaceholder.typicode.com/users/${this.id}/albums`);
  }

  getUser(): Observable<any> {
    return this.RestService.get(`https://jsonplaceholder.typicode.com/users/${this.id}`);
  }

}
