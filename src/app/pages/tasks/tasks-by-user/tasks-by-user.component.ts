import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RestService } from '../../../services/rest.service';
import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-tasks-by-user',
  templateUrl: './tasks-by-user.component.html',
  styleUrls: ['./tasks-by-user.component.scss']
})
export class TasksByUserComponent implements OnInit {
  id:any;
  tasksByUser: Array<Task> = [];

  constructor(private RestService: RestService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getTasksByUser().subscribe(res => this.tasksByUser = res);
  }

  getTasksByUser(): Observable<any>{
    return this.RestService.get(`https://jsonplaceholder.typicode.com/users/${this.id}/todos`)
  }

}
