import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksByUserComponent } from './tasks-by-user.component';

describe('TasksByUserComponent', () => {
  let component: TasksByUserComponent;
  let fixture: ComponentFixture<TasksByUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TasksByUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksByUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
