import { Task } from '../../../models/task';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RestService } from '../../../services/rest.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss']
})

export class TaskDetailComponent implements OnInit {
  id:any;
  task:any = {} as Task;
  
  constructor(private RestService: RestService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getTask().subscribe(res => this.task = res);    
  }

  getTask() : Observable<any> {
    return this.RestService.get(`https://jsonplaceholder.typicode.com/todos/${this.id}`);
  }

  changeTaskState(){
    this.task.completed = !this.task.completed
    console.log(this.task.completed);
  }
}
