import { Task } from '../../models/task';
import { Observable } from 'rxjs';
import { RestService } from '../../services/rest.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  taskList: Array<Task> =[];

  constructor(private RestService: RestService) { }

  ngOnInit(): void {
    this.getTasks().subscribe(res => this.taskList = res);
  }

  getTasks() : Observable<any>{
    return this.RestService.get('https://jsonplaceholder.typicode.com/todos');
  }

}
