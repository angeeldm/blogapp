import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
// import { TasksByUserComponent } from './tasks-by-user/tasks-by-user.component';



@NgModule({
  declarations: [
    // TasksByUserComponent
  ],
  imports: [
    CommonModule,
    BrowserModule
  ]
})
export class TasksModule { }
