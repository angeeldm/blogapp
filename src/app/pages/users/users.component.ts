import { Observable } from 'rxjs';
import { RestService } from '../../services/rest.service';
import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  usersList: Array<User> = [];

  constructor(private RestService: RestService) { }

  ngOnInit(): void {
    this.fetchApi().subscribe(res => {this.usersList = res})
  }

  fetchApi(): Observable<any> {
    return this.RestService.get('https://jsonplaceholder.typicode.com/users');
  }

}
