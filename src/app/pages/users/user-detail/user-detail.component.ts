import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { RestService } from '../../../services/rest.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})

export class UserDetailComponent implements OnInit {
  user:any = {} as User;
  id:any;

  constructor(private RestService: RestService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getUSerById().subscribe(res => this.user = res );
  }

  getUSerById() : Observable <any> {
    return this.RestService.get(`https://jsonplaceholder.typicode.com/users/${this.id}`);
  }
}
