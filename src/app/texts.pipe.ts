import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'texts'
})
export class TextsPipe implements PipeTransform {

  transform(value: string): string {
    if(value.toString().length > 100){
      value = value.substr(0, 100)
    }
    value = value + '...'
    return value;
  }
}
