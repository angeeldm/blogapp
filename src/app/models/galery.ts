export interface Galery{
  albumId:number;
  id:number;
  title:string;
  url:string;
  thumbnailUrl:string;
}