export interface User{
  id:number;
  name:string;
  website:string;
  company:object;
}